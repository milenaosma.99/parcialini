<?php
$titulo= "";
if(isset($_POST["titulo"])){
    $titulo = $_POST["titulo"];
}
$descripcion= "";
if(isset($_POST["descripcion"])){
    $descripcion = $_POST["descripcion"];
}
$fecha = "";
if(isset($_POST["fecha"])){
    $fecha= $_POST["fecha"];
}    
if(isset($_POST["crear"])){
    $articulo = new Articulo("", $titulo, $descripcion, $fecha);
    $articulo -> insertar();    
}

include 'presentacion/menu.php';
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Articulo</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/articulo/crearArticulo.php") ?>" method="post">
						<div class="form-group">
							<label>Titulo</label> 
							<input type="text" name="titulo" class="form-control" value="<?php echo $titulo?>" required>
						</div>
						<div class="form-group">
							<label>Descripcion</label> 
							<input type="text" name="descripcion" class="form-control" value="<?php echo $descripcion?>" required>
						</div>
						<div class="form-group">
							<label>Fecha</label> 
							<input type="date" name="fecha" class="form-control" value="<?php echo $fecha?>" required>
						</div>
						<button type="submit" name="crear" class="btn btn-info">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>