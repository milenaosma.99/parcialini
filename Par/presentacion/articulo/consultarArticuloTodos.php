<?php
$articulo = new Articulo();
$articulos = $articulo -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Articulo</h4>
				</div>
				<div class="text-right"><?php echo count($articulos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Titulo</th>
							<th>Descripcion</th>
							<th>Fecha</th>
						</tr>
						<?php 
						$i=1;
						foreach($articulos as $articuloActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $articuloActual -> getTitulo() . "</td>";
						    echo "<td>" . $articuloActual -> getDescripcion() . "</td>";
						    echo "<td>" . $articuloActual -> getFecha() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item disabled"><span class="page-link"> &lt;&lt; </span>
        						</li>
        						<li class="page-item"><a class="page-link" href="index.php?pid=<?php echo base64_encode("presentacion/articulo/consultarArticuloTodos.php") ?>&pagina=1">1</a></li>
        						<li class="page-item active" aria-current="page"><span
        							class="page-link"> 2 <span class="sr-only">(current)</span>
        						</span></li>
        						<li class="page-item"><a class="page-link" href="#">3</a></li>
        						<li class="page-item"><a class="page-link" href="#"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>