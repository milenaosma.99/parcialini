<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ArticuloDAO.php";
class Articulo{
    private $idarticulo;
    private $titulo;
    private $descripcion;
    private $fecha;
    private $conexion;
    private $articuloDAO;
    
    public function getIdarticulo(){
        return $this -> idarticulo;
    }
    
    public function getTitulo(){
        return $this -> titulo;
    }
    
    public function getDescripcion(){
        return $this -> descripcion;
    }
    
    public function getFecha(){
        return $this -> fecha;
    }
        
    public function Articulo($idarticulo= "", $titulo = "", $descripcion= "", $fecha= ""){
        $this -> idarticulo = $idarticulo;
        $this -> titulo = $titulo;
        $this -> descripcion = $descripcion;
        $this -> fecha = $fecha;
        $this -> conexion = new Conexion();
        $this -> ArticuloDAO = new ArticuloDAO($this -> idarticulo, $this -> titulo, $this -> descripcion, $this -> fecha);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> articuloDAO-> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO-> consultarTodos());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($articulos, $p);
        }
        $this -> conexion -> cerrar();        
        return $articulos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> articuloDAO-> consultarPaginacion($cantidad, $pagina));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($articulos, $p);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

?>