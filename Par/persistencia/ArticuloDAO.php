<?php
class ArticuloDAO{
    private $idarticulo;
    private $titulo;
    private $descripcion;
    private $fecha;
       
    public function ArticuloDAO($idarticulo= "", $titulo= "", $descripcion= "", $fecha= ""){
        $this -> idarticulo = $idarticulo;
        $this -> titulo = $titulo;
        $this -> descripcion = $descripcion;
        $this -> fecha = $fecha;
    }
       
    public function insertar(){
        return "insert into Articulo (titulo, descripcion, fecha)
                values ('" . $this -> titulo . "', '" . $this -> descripcion. "', '" . $this -> fecha. "')";
    }
    
    public function consultarTodos(){
        return "select idarticulo, titulo, descripcion, fecha
                from Articulo";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idarticulo, titulo, descripcion, fecha
                from Articulo
                limit " . (($pagina-1) * $cantidad) . " , " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idarticulo)
                from Articulo";
    }
    
}

?>